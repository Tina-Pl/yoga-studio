$(document).ready(function () {
  // Smooth Scrolling
  var scrollLink = $('.scroll-link');

  scrollLink.click(function (event) {
    event.preventDefault();
    var hash = this.hash;

    $('body, html').animate(
      {
        scrollTop: $(hash).offset().top,
      },
      1000,
      function () {
        window.location.hash = hash;
      }
    );
  });

  //Logo & Home Link Scroll
  $('#logo, #home').on('click', function () {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      1000
    );
  });

  // Menu toggle
  $('.toggle-btn').click(function () {
    $('nav ul').toggleClass('active');
    $('nav').toggleClass('white-bg');
  });

  // Sticky Nav
  $(window).scroll(function () {
    var windowTop = $(window).scrollTop();
    var aboutSectionOffset = $('#about').offset().top;

    if (windowTop >= aboutSectionOffset - 60) {
      $('nav').addClass('sticky');
    } else {
      $('nav').removeClass('sticky');
    }
  });

  //Animations on Scroll
  AOS.init({
    // delay AOS initial calculations
    startEvent: 'load',
  });

  // Slick Slider
  $('.slide').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 850,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1,
        },
      },
    ],
  });
});

// Location Map

var map = L.map('map-box').setView([40.78502575952242, -73.97914409637451], 13);

L.tileLayer(
  'https://tile.jawg.io/jawg-streets/{z}/{x}/{y}.png?access-token=j3Fu9mhUVpV8My6DPgoWNcdpMgrHOIGmMYaVgGKbpmeuizOQofI33x4w91u7NL5v',
  {
    maxZoom: 13,
    attribution:
      '<a href="https://www.jawg.io" target="_blank">&copy; Jawg</a> | <a href="https://www.openstreetmap.org" target="_blank">&copy; OpenStreetMap</a>&nbsp;contributors',
  }
).addTo(map);

L.marker([40.78502575952242, -73.97914409637451]).addTo(map);
