!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> 
    
    <link rel="stylesheet" type="text/css" href="css/join_us.css">
    <link rel="stylesheet" type="text/css" href="css/queries.css">
    <title>Lorem Yoga Studio</title>
   
</head>
<body>
    <?php
        // Define variables for form data and error messages - empty values
        $firstName = $lastName = $email = $yogaClass = $comment = "";
        $firstNameErr = $lastNameErr = $emailErr = $classErr = "";

        // Check if the submit button was clicked
        if(isset($_POST['submit'])) {
            // Check if the variable is empty (did the user enter data in the input?)
            if(empty($_POST['firstname'])) {
                // error message
                $firstNameErr = "First name is required.";
            } else {
                $firstName = $_POST['firstname'];
            }

            if(empty($_POST['lastname'])) {
                // error message
                $lastNameErr = "Last name is required.";
            } else {
                $lastName = $_POST['lastname'];
            }

            if(empty($_POST['email'])) {
                // error message
                $emailErr = "Email is required.";
            } else {
                $email = $_POST['email'];
                // check if the userr entered a valid email format
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    // error message
                    $emailErr = "Write a valid e-mail address!";
                }
            }

            if(empty($_POST['comment'])) {
                // no error message, leave textarea empty (not required field)
                $comment = "";
            } else {
                $comment = $_POST['comment'];
            }

            if(empty($_POST['classes'])) {
                // no error message, leave the default option
                $classErr = "";
            } else {
                $yogaClass = $_POST['classes'];
            }
        }
    ?>

    <!--======== Navbar ========-->
    <nav>
        <div class="nav-row">
            <div class="toggle-menu">
                <a href="index.html#home" id="logo"><img class="logo" src="img/logo.png" alt="Logo"></a>
                <a class="toggle-btn"><i class="ion-navicon"></i></a>
            </div>
            <ul>
                <li><a href="index.html#home" id="home">Home</a></li>
                <li><a href="index.html#about">Welcome</a></li>
                <li><a href="index.html#classes">Classes</a></li>
                <li><a href="index.html#teachers">Teachers</a></li>
                <li><a href="index.html#schedule">Schedule</a></li>
                <li><a href="index.html#prices">Pricing</a></li>
                <li><a href="index.html#contact">Contact</a></li>
            </ul>
        </div>
    </nav>

    <!--======== Form ========-->
    <main>    
        <div class="padding">
            <h2>Join us</h2>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis, eos et nesciunt 
                rem suscipit alias aut nemo animi. Aliquid doloremque impedit ducimus reiciendis, 
                dolore quasi explicabo facere vel ratione iste!Lorem ipsum dolor sit amet consectetur 
                adipisicing elit. Voluptas, ex? Sit beatae libero esse omnis, qui sequi maiores! Saepe, 
                aut expedita enim praesentium tempore perspiciatis fugiat voluptates veniam atque nemo.</p>
            
            <div class="container">
                <h3>Get your Free Trail Class</h3>
                <p>You are warmly invited to join us for a free class of your choice!</p>
                <form action="<?php echo ($_SERVER['PHP_SELF']); ?>" method="post">
                    <div class="row">
                        <div class="column-2">
                            <input type="text" name="firstname" placeholder="First name" value="<?php echo $firstName; ?>"><span class="error">* </span>
                            <p class="error-message">&nbsp;<?php echo $firstNameErr; ?></p>
                            <input type="text" name="lastname" placeholder="Last name" value="<?php echo $lastName; ?>"><span class="error">* </span>
                            <p class="error-message">&nbsp;<?php echo $lastNameErr; ?></p>
                            <input type="email" name="email" placeholder="Email" value="<?php echo $email; ?>"><span class="error">* </span>
                            <p class="error-message">&nbsp;<?php echo $emailErr; ?></p>
                        </div>
                        <div class="column-2">
                            <label for="classes">Choose a class:</label>
                            <select name="classes" id="classes">
                                <option <?php if(isset($yogaClass) && $yogaClass == "Vinyasa Yoga") echo "selected" ;?> value="Vinyasa Yoga">Vinyasa Yoga</option>
                                <option <?php if(isset($yogaClass) && $yogaClass == "Yin Yoga") echo "selected" ;?> value="Yin Yoga">Yin Yoga</option>
                                <option <?php if(isset($yogaClass) && $yogaClass == "Hot Yoga") echo "selected" ;?> value="Hot Yoga">Hot Yoga</option>
                                <option <?php if(isset($yogaClass) && $yogaClass == "Pregnancy Yoga") echo "selected" ;?> value="Pregnancy Yoga">Pregnancy Yoga</option>
                                <option <?php if(isset($yogaClass) && $yogaClass == "Kids Yoga") echo "selected" ;?> value="Kids Yoga">Kids Yoga</option>
                                <option <?php if(isset($yogaClass) && $yogaClass == "Meditation") echo "selected" ;?> value="Meditation">Meditation</option>
                            </select><span class="error">* </span>
                            <br>
                            <textarea name="comment" placeholder="Comment..." <?php echo $comment; ?>></textarea>
                            <p class="required">* required fields</p>
                        </div>
                        <input id="submit-btn" type="submit" name="submit" value="Join Today!">
                    </div>
                </form>
                <p class="submit-message">
                    <?php if($firstName != '' && $lastName != '' && $email != ''): ?>
                        <h4>Hello, <span class="name"><?php echo $firstName; ?></span> <span class="last-name"><?php echo $lastName; ?></span>!</h4>
                        <p>We are excited you decided to join us for a free trial <span class="yoga-class"><?php echo $yogaClass; ?></span> class. 
                        You will recieve a confirmation e-mail with some additional information about the yoga class to the following e-mail address 
                        you provided: <span class="email"><?php echo $email; ?></span>.
                        <br>
                        <br>
                        Namaste, <br>
                        <em>Lorem Yoga Studio Team</em></p>
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="script.js"></script>
</body>
