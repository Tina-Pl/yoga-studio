# Yoga Studio Website
---

This is another personal project of mine. The main goal here was to build the website using CSS Grid, Flexbox, jQuery and PHP. This time it's not a single page website, because I decided to include a simple form that open in a new page. Here I used PHP for form validation, to retrieve data from the form and to display a short message after it is submitted.  
I also wanted to try out and implement some JavaScript libraries and jQuery plugin into this website - I used AOS.js to animate certain content/elements on scroll, Leaflet to add a map and Slick Slider for a responsive slider.  

## Built With
---
* HTML5
* CSS3
* PHP
* jQuery
* [Slick Slider](http://kenwheeler.github.io/slick/) - jQuery carousel plugin
* [AOS.js](https://michalsnik.github.io/aos/) - Animate on scroll library
* [Leaflet](https://leafletjs.com/) - JavaScript library for interactive maps
* [Leaflet-Extras/Leaflet Provideres](https://leaflet-extras.github.io/leaflet-providers/preview/) -    Leaflet base map plugin
* [Ionicons](https://ionicons.com/v2) - Icons
* [Pixabay](https://pixabay.com/) - Photos
* [Pexels](https://www.pexels.com/) - Photos
